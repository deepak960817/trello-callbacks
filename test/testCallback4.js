const getInformation = require('../callback4.js');

// Importing main function of callback1 problem
const obj1 = require('../callback1.js');
const getBoardInformation = obj1.getBoardInformation;

// Importing main function of callback2 problem
const obj2 = require('../callback2.js');
const getListInformation = obj2.getListInformation;

// Importing callback function of callback3 problem
const obj3 = require('../callback3.js');
const getCardInformation = obj3.getCardInformation;

getInformation(getBoardInformation, getListInformation, getCardInformation,"Thanos");