// Importing data files
const boards = require('./data/boards.json');
const cards = require('./data/cards.json');
const lists = require('./data/lists.json');

// Importing callback function of callback1 problem
const obj1 = require('./callback1.js');
const boardData = obj1.boardData;

// Importing callback function of callback2 problem
const obj2 = require('./callback2.js');
const listData = obj2.listData;

// Importing callback function of callback3 problem
const obj3 = require('./callback3.js');
const cardData = obj3.cardData;

// Main function to get information of Thanos
function getInformation(getBoardInformation, getListInformation, getCardInformation, name)
{
    let boardID = boards.reduce((accumulator, current) => {
        
        if(current["name"] === name)
        {
            accumulator = current["id"];
        }
        return accumulator;
    },[])

        setTimeout(() => {
            
            // Getting Board Information of Thanos
            getBoardInformation(boardData, boardID)
            setTimeout(() => {

                // Getting List Information for Thanos
                getListInformation(listData, boardID)
                setTimeout(() => {

                    // Checking and storing card ids with name Mind
                    const cardName = Object.entries(lists).reduce((accumulator, current) => {
                        for(let index=0; index<current[1].length; index++)
                        {
                            if(current[1][index]["name"] === "Mind")
                            {
                                accumulator = current[1][index]["id"];
                            }
                        }
                            return accumulator;
                    },[])

                    // Getting card Information
                    getCardInformation(cardData, cardName[0]);

                },4000)

            },4000)
            
        },2000)
}

module.exports = getInformation;