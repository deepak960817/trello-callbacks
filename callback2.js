const boards = require('./data/boards.json');
const cards = require('./data/cards.json');
const lists = require('./data/lists.json');

// Callback function to get the data
function listData(boardID, lists)
{
    const listInfo = Object.entries(lists).reduce((accumulator, current) => {
        
        if(current[0] === boardID)
        {
            accumulator = current[1];
        } 
        return accumulator;
    },[]);

    return listInfo;
}

// Main function calling callback function with parameters.
function getListInformation(callback, boardID)
{
    if(typeof boardID !== 'string')
    {
        return [];
    }
    console.log("Fetching List Information");

    let listInfo;
    setTimeout(() => {
        
        listInfo = callback(boardID, lists);
        console.log(listInfo);

        console.log("List Information Displayed");

        return listInfo;
    },2000)
}

module.exports = {getListInformation, listData};