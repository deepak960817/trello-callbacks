const boards = require('./data/boards.json');
const cards = require('./data/cards.json');
const lists = require('./data/lists.json');

// Callback function to get the data
function cardData(listID, cards)
{
    const cardInfo = Object.entries(cards).reduce((accumulator, current) => {
        if(current[0] === listID)
        {
            accumulator = current;
        }
        return accumulator;
    })

    return cardInfo;
}

// Main function calling callback function with parameters.
function getCardInformation(callback, listID)
{
    if(typeof listID !== 'string')
    {
        return [];
    }
    console.log("Fetching Card Information");

    let cardInfo;
    setTimeout(() => {
        
        cardInfo = callback(listID, cards);
        console.log(cardInfo);

        console.log("Card Information Displayed");

        return cardInfo;
    },2000)
}

module.exports = {getCardInformation, cardData};