const boards = require('./data/boards.json');
const cards = require('./data/cards.json');
const lists = require('./data/lists.json');

// Callback function to get the data
function boardData(boardID, boards)
{
    const boardInfo = boards.reduce((accumulator, current) => {
        if(current["id"] == boardID)
        {
            accumulator.push(current);
        }
        return accumulator;
    },[]);

    return boardInfo;
}

// Main function calling callback function with parameters.
function getBoardInformation(callback, boardID)
{
    if(typeof boardID !== 'string')
    {
        return [];
    }
    console.log("Fetching Board Information");

    setTimeout(() => {
        
        const boardInfo = callback(boardID, boards);
        console.log(boardInfo);

        console.log("Board Information Displayed");

        return boardInfo;
    },2000)
}

module.exports = {getBoardInformation, boardData};
